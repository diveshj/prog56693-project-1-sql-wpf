﻿using Microsoft.Data.Sqlite;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PROG56693_Project_1_SQL_WPF
{

    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
            TopText.Text = "The 5 data stored in SQL are \n 1. MatchTime - How long each match has lasted. \n 2. pDestroyedbyCollision - How many times the player lost a life due " +
        "to collision with Enemy Ship or Asteroid. \n 3. pDeathCounter - How many times player has died. \n 4. pDestroyedEnemies - How many times the player shot down an enemy. \n " +
        "5. pDestroyedAsteroids - How many times player destroyed an asteroid. \n Also contains id for each round played";
        }

        private void MenuLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Database (*.db)|*.db";
            if (dlg.ShowDialog() == true)
            {
                string dbFile = dlg.FileName;
                SpaceData.ItemsSource = SpaceShooterData.ReadData(dbFile);

            }
        }
    }


    //Data for my Space Shooter gets loaded here. 
    class SpaceShooterData
    {
       public float MatchTime { get; set; }
        public int pDestroyedbyCollision { get; set; }
        public int pDeathCounter { get; set; }
        public int pDestroyedEnemies { get; set; }
        public int pDestroyedAsteroids { get; set; }
        public int id { get; set; }

        public static List<SpaceShooterData> ReadData(string filepath)
        {
            List<SpaceShooterData> Data = new List<SpaceShooterData>();
            try
            {
                string connectionString = "Data Source =" + filepath;
                using (SqliteConnection connection = new SqliteConnection(connectionString))
                {
                    connection.Open();
                    SqliteCommand dbcmd = connection.CreateCommand();
                    dbcmd.CommandText = "SELECT * FROM SpaceShooterData;";
                    using (var reader = dbcmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SpaceShooterData data = new SpaceShooterData();
                            data.id = reader.GetInt32(0);
                            data.MatchTime = reader.GetFloat(1);
                            data.pDestroyedbyCollision = reader.GetInt32(2);
                            data.pDeathCounter = reader.GetInt32(3);
                            data.pDestroyedEnemies = reader.GetInt32(4);
                            data.pDestroyedAsteroids = reader.GetInt32(5);
                            Data.Add(data);
                            
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            return Data;
        }

    }
}
