# SpaceShooter

Tool to browse SQL data generated during gameplay with [SpaceShooter](https://gitlab.com/diveshj/spaceshooter).


```
Open SpaceShooter/Assets/Database/spaceshooterdb.db using the tool
```
# Images
![SQL Data](https://gitlab.com/diveshj/project-images-dump/-/raw/main/SpaceShooter%20SQL%20WPF/dbimage.jpg)
